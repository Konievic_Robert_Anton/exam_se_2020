import java.util.concurrent.TimeUnit;

public class S2 {
    public static void main(String[] args) {
        S LThread1=new S("LThread1");
        S LThread2=new S("LThread2");
        S LThread3=new S("LThread3");
        LThread1.start();
        LThread2.start();
        LThread3.start();
    }
}

class S extends Thread{

    String name;

    S(String name){
    this.name=name;
    }

    @Override
    public void run() {
        for(int i=0;i<6;i++){
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name+"-"+java.time.LocalTime.now());
        }
    }
}

